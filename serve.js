var port = process.env.PORT || 8888;

var finalhandler = require('finalhandler'),
http = require('http'),
url = require('url'),
serveStatic = require('serve-static'),
qs = require('querystring'),
fs = require('fs');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var mongoUrl = "mongodb://message_user:FvO00y8r1365.R+=-i|N@ds031751.mongolab.com:31751/messages";
var serve = serveStatic('public/', {'index': ['index.html', 'index.htm']})

var server = http.createServer(function(req, res){
	var pathname = url.parse(req.url).pathname;
	if (pathname == '/store'){
		var body = '';
        req.on('data', function (data) {
        	console.log('getting data!');
            body += data;

            // Too much POST data, kill the connection!
            if (body.length > 1e6)
                req.connection.destroy();
        });

        req.on('end', function () {
            var post = qs.parse(body);
            console.log(post);
            var message = post['message'];
            var email = post['email'];
            var now = new Date().getTime();
			     now = now / 1000 | 0;

            messageJSON = {
            	"message" : message,
            	"email" : email,
            	"timestamp" : now
            };
            mongoStore(messageJSON);
        });
	}
	if (pathname == '/messages.txt'){
		res.writeHead(200,'OK');
		var newMessages = [];
		mongoRead(function(newMessages){
			for(var i=0;i<newMessages.length;i++)
			{
				res.write(newMessages[i]+'\n');

			}
			fs.readFile('./messages.txt','utf8',function(err,file){
				res.end(file);
			})
			//res.end();
		})
	}
  	var done = finalhandler(req, res);
  	serve(req, res, done);
})
server.listen(port);

function mongoStore(message){
	MongoClient.connect(mongoUrl, function(err, db) {
    if (err){
      console.log('Could not connect to DB.');
      return false;
    }
  		insertMessages(db, message, function(){
  			db.close();
  		});
	});
};
insertMessages = function(db, messageJSON, callback) {
	console.log('adding to db!');
	console.log(messageJSON);
	var collection = db.collection('messages');
  	collection.insert([messageJSON], function(err, result) {
    	assert.equal(err, null);
    	callback(result);
  	});
}
function mongoRead(callback){
	MongoClient.connect(mongoUrl, function(err, db) {
		if(err){
			console.log('theres a problem');
		}
  		assert.equal(null, err);
  		var collection = db.collection('messages');
		var now = new Date().getTime();
		now = now / 1000 | 0;
		date = now - 60 * 60 * 12;

  		collection.find().toArray(function(err,docs){
  			if(!err){
  				db.close();
  				var myArray = [];
  				for (var i = docs.length - 1; i >= 0; i--) {
  					myArray.push(docs[i].message);
  				};
  				callback(myArray);
  			}
  		});
  	});
}